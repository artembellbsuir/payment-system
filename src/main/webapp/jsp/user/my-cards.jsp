<%@ page import="by.artem.payme.bean.Card" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: artembell
  Date: 13.12.2020
  Time: 10:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>My cards</title>
</head>
<body>
    <h1>My cards</h1>

    <a href="controller?command=create-card">create card</a>

    <p style = "color:red;"><%=request.getAttribute("error-text")%></p>

    <%
        ArrayList<Card> cards = (ArrayList<Card>)request.getAttribute("cards");
        for(Card card : cards) {%>

        <p>
            <h4>Account number: <%=card.getAccountNumber()%></h4>
            <h4>Number: <%=card.getNumber()%></h4>
        </p>
        <hr>
    <%}%>
</body>
</html>
