package by.artem.payme.command.impl;

import by.artem.payme.command.Command;
import by.artem.payme.command.exception.CommandException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CreatePaymentCommand implements Command {
   @Override
   public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, CommandException {
      response.sendRedirect("controller?command=create-account");
   }
}
