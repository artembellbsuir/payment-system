package by.artem.payme.command.impl;

import by.artem.payme.command.Command;
import by.artem.payme.command.exception.CommandException;
import by.artem.payme.controller.ActionUrlName;
import by.artem.payme.controller.RequestParameterName;
import by.artem.payme.service.ServiceFactory;
import by.artem.payme.service.UserService;
import by.artem.payme.service.exception.ServiceException;
import by.artem.payme.service.impl.FieldValidatorServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginationCommand implements Command {

   @Override
   public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
      String username = request.getParameter(RequestParameterName.LOGIN_USERNAME);
      String password = request.getParameter(RequestParameterName.LOGIN_PASSWORD);

      ServiceFactory serviceFactory = ServiceFactory.getInstance();
      UserService userService = serviceFactory.getUserService();

      boolean loginResult,
              validationResult = FieldValidatorServiceImpl.validateString(username)
                      && FieldValidatorServiceImpl.validatePassword(password);

      if (validationResult) {
         try {
            loginResult = userService.login(username, password);
            if (loginResult) {
               HttpSession session = request.getSession();
//               User user = userService.getUserByUsername(username);
               session.setAttribute("username", username);

               response.sendRedirect(ActionUrlName.COMMAND_MY_ACCOUNTS);
            } else {
               request.setAttribute("error-text", "Login error, try again");
               response.sendRedirect(ActionUrlName.COMMAND_LOGIN);
            }
         } catch (ServiceException e) {
            request.setAttribute("error-text", e.getMessage());
            request.getRequestDispatcher(ActionUrlName.PAGE_ERROR).forward(request, response);
         }
      } else {
         request.setAttribute("error-text", "Fields validation error, try again");
         request.getRequestDispatcher(ActionUrlName.PAGE_LOGIN).forward(request, response);
      }
   }
}