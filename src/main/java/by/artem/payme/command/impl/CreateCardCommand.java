package by.artem.payme.command.impl;

import by.artem.payme.bean.User;
import by.artem.payme.command.Command;
import by.artem.payme.command.exception.CommandException;
import by.artem.payme.controller.ActionUrlName;
import by.artem.payme.controller.RequestParameterName;
import by.artem.payme.service.CardService;
import by.artem.payme.service.ServiceFactory;
import by.artem.payme.service.UserService;
import by.artem.payme.service.exception.ServiceException;
import by.artem.payme.service.impl.ValueGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class CreateCardCommand implements Command {
   @Override
   public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
      String accountNumberField = request.getParameter(RequestParameterName.CREATE_CARD_ACCOUNT_NUMBER);

      ServiceFactory serviceFactory = ServiceFactory.getInstance();
      UserService userService = serviceFactory.getUserService();
      CardService cardService = serviceFactory.getCardService();

      boolean createAccountResult,
              validationResult = accountNumberField != null;


      if (validationResult) {
         try {
            HttpSession session = request.getSession();
            String username = (String) session.getAttribute("username");
            User user = userService.getUserByUsername(username);

            int accountNumber = ValueGenerator.generateNumber();
            int cardNumber = ValueGenerator.generateNumber();

            if (username != null) {
               createAccountResult = cardService.createCard(user.getId(), accountNumber, cardNumber);
               if (createAccountResult) {
                  response.sendRedirect(ActionUrlName.COMMAND_MY_CARDS);
               } else {
                  request.setAttribute("error-text", "Creating card error, try again");
                  response.sendRedirect(ActionUrlName.COMMAND_CREATE_CARD);
               }
            } else {
               response.sendRedirect(ActionUrlName.COMMAND_LOGIN);
            }
         } catch (ServiceException e) {
            request.setAttribute("error-text", e.getMessage());
            request.getRequestDispatcher(ActionUrlName.PAGE_ERROR).forward(request, response);
         }
      } else {
         request.setAttribute("error-text", "Fields validation error, try again");
         request.getRequestDispatcher(ActionUrlName.PAGE_CREATE_CARD).forward(request, response);
      }
   }
}
