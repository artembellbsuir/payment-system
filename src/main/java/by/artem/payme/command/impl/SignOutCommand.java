package by.artem.payme.command.impl;

import by.artem.payme.command.Command;
import by.artem.payme.command.exception.CommandException;
import by.artem.payme.controller.ActionUrlName;
import by.artem.payme.service.ServiceFactory;
import by.artem.payme.service.UserService;
import by.artem.payme.service.exception.ServiceException;
import by.artem.payme.service.impl.FieldValidatorServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class SignOutCommand implements Command {
   @Override
   public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, CommandException, ServiceException {
      HttpSession session = request.getSession();
      session.removeAttribute("username");
      request.getRequestDispatcher(ActionUrlName.PAGE_LOGIN).forward(request, response);
   }
}
