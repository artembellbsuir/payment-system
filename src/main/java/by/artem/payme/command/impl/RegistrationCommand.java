package by.artem.payme.command.impl;

import by.artem.payme.command.Command;
import by.artem.payme.controller.ActionUrlName;
import by.artem.payme.controller.RequestParameterName;
import by.artem.payme.service.ServiceFactory;
import by.artem.payme.service.UserService;
import by.artem.payme.service.exception.ServiceException;
import by.artem.payme.service.impl.FieldValidatorServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RegistrationCommand implements Command {
   @Override
   public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
      String username = request.getParameter(RequestParameterName.REGISTER_USERNAME);
      String firstName = request.getParameter(RequestParameterName.REGISTER_FIRST_NAME);
      String lastName = request.getParameter(RequestParameterName.REGISTER_LAST_NAME);
      String email = request.getParameter(RequestParameterName.REGISTER_EMAIL);
      String password = request.getParameter(RequestParameterName.REGISTER_PASSWORD);

      ServiceFactory serviceFactory = ServiceFactory.getInstance();
      UserService userService = serviceFactory.getUserService();

      boolean registrationResult,
              validationResult = FieldValidatorServiceImpl.validateString(username)
                      && FieldValidatorServiceImpl.validatePassword(password)
                      && FieldValidatorServiceImpl.validateString(firstName)
                      && FieldValidatorServiceImpl.validateString(lastName)
                      && FieldValidatorServiceImpl.validateString(email);

      if (validationResult) {
         try {
            registrationResult = userService.register(username, firstName, lastName, email, password);
            if (registrationResult) {
               response.sendRedirect(ActionUrlName.COMMAND_LOGIN);
            } else {
               request.setAttribute("error-text", "Register error, try again");
               response.sendRedirect(ActionUrlName.COMMAND_REGISTER);
            }
         } catch (ServiceException e) {
            request.setAttribute("error-text", e.getMessage());
            request.getRequestDispatcher(ActionUrlName.PAGE_ERROR).forward(request, response);
         }
      } else {
         request.setAttribute("error-text", "Fields validation error");
         request.getRequestDispatcher(ActionUrlName.PAGE_REGISTRATION).forward(request, response);
      }
   }
}
