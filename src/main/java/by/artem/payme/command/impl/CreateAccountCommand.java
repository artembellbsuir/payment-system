package by.artem.payme.command.impl;

import by.artem.payme.bean.User;
import by.artem.payme.command.Command;
import by.artem.payme.command.exception.CommandException;
import by.artem.payme.controller.ActionUrlName;
import by.artem.payme.controller.RequestParameterName;
import by.artem.payme.service.AccountService;
import by.artem.payme.service.ServiceFactory;
import by.artem.payme.service.UserService;
import by.artem.payme.service.exception.ServiceException;
import by.artem.payme.service.impl.FieldValidatorServiceImpl;
import by.artem.payme.service.impl.ValueGenerator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class CreateAccountCommand implements Command {
   @Override
   public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, CommandException, ServiceException {
      String balance = request.getParameter(RequestParameterName.CREATE_ACCOUNT_BALANCE);

      ServiceFactory serviceFactory = ServiceFactory.getInstance();
      UserService userService = serviceFactory.getUserService();
      AccountService accountService = serviceFactory.getAccountService();

      boolean createAccountResult,
              validationResult = FieldValidatorServiceImpl.validateNumber(balance);

      if (validationResult) {
         try {
            HttpSession session = request.getSession();
            String username = (String) session.getAttribute("username");

            User user = userService.getUserByUsername(username);

            if (username != null) {
               int accountNumber = ValueGenerator.generateNumber();
               createAccountResult = accountService.createAccount(user.getId(), accountNumber, Integer.parseInt(balance), false);
               if (createAccountResult) {
                  response.sendRedirect(ActionUrlName.COMMAND_MY_ACCOUNTS);
               } else {
                  request.setAttribute("error-text", "Creating account error, try again");
                  response.sendRedirect(ActionUrlName.COMMAND_CREATE_ACCOUNT);
               }
            } else {
               response.sendRedirect(ActionUrlName.COMMAND_LOGIN);
            }
         } catch (ServiceException e) {
            request.setAttribute("error-text", e.getMessage());
            request.getRequestDispatcher(ActionUrlName.PAGE_ERROR).forward(request, response);
         }
      } else {
         request.setAttribute("error-text", "Fields validation error, try again");
         request.getRequestDispatcher(ActionUrlName.PAGE_CREATE_ACCOUNT).forward(request, response);
      }
   }
}

