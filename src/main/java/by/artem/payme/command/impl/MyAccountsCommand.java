package by.artem.payme.command.impl;

import by.artem.payme.bean.Account;
import by.artem.payme.bean.User;
import by.artem.payme.command.Command;
import by.artem.payme.command.exception.CommandException;
import by.artem.payme.controller.ActionUrlName;
import by.artem.payme.service.AccountService;
import by.artem.payme.service.ServiceFactory;
import by.artem.payme.service.UserService;
import by.artem.payme.service.exception.ServiceException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

public class MyAccountsCommand implements Command {
   @Override
   public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, CommandException, ServiceException {
      ServiceFactory serviceFactory = ServiceFactory.getInstance();
      UserService userService = serviceFactory.getUserService();
      AccountService accountService = serviceFactory.getAccountService();

      HttpSession session = request.getSession();
      String username = (String) session.getAttribute("username");

      if (username != null) {
         try {
            User user = userService.getUserByUsername(username);
            ArrayList<Account> accounts = accountService.getAccountsByCustomerId(user.getId());

            request.setAttribute("accounts", accounts);
            request.getRequestDispatcher(ActionUrlName.PAGE_MY_ACCOUNTS).forward(request, response);
         } catch (ServiceException e) {
            request.setAttribute("error-text", e.getMessage());
            request.getRequestDispatcher(ActionUrlName.PAGE_ERROR).forward(request, response);
         }
      } else {
         response.sendRedirect(ActionUrlName.COMMAND_LOGIN);
      }
   }
}
