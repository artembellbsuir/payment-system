package by.artem.payme.command;

import by.artem.payme.command.exception.CommandException;
import by.artem.payme.service.exception.ServiceException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface Command {

   void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, CommandException, ServiceException;
}
