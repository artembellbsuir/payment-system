package by.artem.payme.command;

public final class CommandType {
   private CommandType() {}

   public static final String LOGIN = "login";
   public static final String REGISTER = "register";
   public static final String MY_ACCOUNTS = "my-accounts";
   public static final String MY_CARDS = "my-cards";
   public static final String CREATE_ACCOUNT = "create-account";
   public static final String CREATE_CARD = "create-card";
   public static final String SIGNOUT = "signout";
}
