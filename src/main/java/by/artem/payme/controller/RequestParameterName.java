package by.artem.payme.controller;

public final class RequestParameterName {
   private RequestParameterName() {
   }

   public static final String COMMAND_NAME = "command";

   public static final String LOGIN_USERNAME = "username";
   public static final String LOGIN_PASSWORD = "password";

   public static final String REGISTER_USERNAME = "username";
   public static final String REGISTER_FIRST_NAME = "firstName";
   public static final String REGISTER_LAST_NAME = "lastName";
   public static final String REGISTER_EMAIL = "email";
   public static final String REGISTER_PASSWORD = "password";

   public static final String CREATE_ACCOUNT_BALANCE = "balance";

   public static final String CREATE_CARD_ACCOUNT_NUMBER = "accountNumber";

}
