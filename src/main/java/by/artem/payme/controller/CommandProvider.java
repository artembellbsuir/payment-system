package by.artem.payme.controller;

import by.artem.payme.command.Command;
import by.artem.payme.command.CommandType;
import by.artem.payme.command.impl.*;

import java.util.HashMap;
import java.util.Map;

final class CommandProvider {
   final private Map<String, Command> commands = new HashMap<>();

   CommandProvider() {
      commands.put(CommandType.LOGIN, new LoginationCommand());
      commands.put(CommandType.REGISTER, new RegistrationCommand());
      commands.put(CommandType.MY_ACCOUNTS, new MyAccountsCommand());
      commands.put(CommandType.MY_CARDS, new MyCardsCommand());
      commands.put(CommandType.CREATE_ACCOUNT, new CreateAccountCommand());
      commands.put(CommandType.CREATE_CARD, new CreateCardCommand());
      commands.put(CommandType.SIGNOUT, new SignOutCommand());
   }

   Command getCommand(String commandName) {
      return commands.get(commandName);
   }

}
