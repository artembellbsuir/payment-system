package by.artem.payme.controller;

public final class ActionUrlName {
   private ActionUrlName() {
   }

   // pages
   public static final String PAGE_ERROR = "/jsp/user/error.jsp";
   public static final String PAGE_REGISTRATION = "/jsp/user/register.jsp";
   public static final String PAGE_LOGIN = "/jsp/user/login.jsp";
   public static final String PAGE_MY_ACCOUNTS = "/jsp/user/my-accounts.jsp";
   public static final String PAGE_MY_CARDS = "/jsp/user/my-cards.jsp";
   public static final String PAGE_CREATE_ACCOUNT = "/jsp/user/create-account.jsp";
   public static final String PAGE_CREATE_CARD = "/jsp/user/create-card.jsp";
   public static final String PAGE_MAIN = "index.jsp";

   // commands
   public static final String COMMAND_LOGIN = "controller?command=login";
   public static final String COMMAND_REGISTER = "controller?command=register";
   public static final String COMMAND_MY_ACCOUNTS = "controller?command=my-accounts";
   public static final String COMMAND_MY_CARDS = "controller?command=my-cards";
   public static final String COMMAND_CREATE_ACCOUNT = "controller?command=create-account";
   public static final String COMMAND_CREATE_CARD = "controller?command=create-card";
   public static final String COMMAND_SIGNOUT = "controller?command=signout";
}
