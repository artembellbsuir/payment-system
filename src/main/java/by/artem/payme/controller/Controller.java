package by.artem.payme.controller;

import by.artem.payme.command.Command;
import by.artem.payme.command.exception.CommandException;
import by.artem.payme.service.exception.ServiceException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class Controller extends HttpServlet {

   private final CommandProvider provider = new CommandProvider();

   protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      doAction(request, response);
   }

   protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      doAction(request, response);
   }

   private void doAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      String commandName = request.getParameter("command");
      Command executionCommand = provider.getCommand(commandName);

      try {
         executionCommand.execute(request, response);
      } catch (CommandException | ServiceException e) {
         e.printStackTrace();
      }
   }
}