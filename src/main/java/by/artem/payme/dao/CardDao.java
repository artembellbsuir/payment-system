package by.artem.payme.dao;

import by.artem.payme.bean.Account;
import by.artem.payme.bean.Card;
import by.artem.payme.dao.exception.DAOException;

import java.util.ArrayList;

public interface CardDao {
   boolean createCard(int customerId, int accountNumber, int number) throws DAOException;

   ArrayList<Card> getBankAccountCards(int accountNumber) throws DAOException;

   ArrayList<Card> getCardsByCustomerId(int customerId) throws DAOException;

   Card getCardByCardNumber(int cardNumber) throws DAOException;

   boolean deleteCardById(int id) throws DAOException;
}
