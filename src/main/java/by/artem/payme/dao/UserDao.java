package by.artem.payme.dao;

import by.artem.payme.bean.User;
import by.artem.payme.dao.exception.DAOException;

import java.sql.SQLException;

public interface UserDao {

   boolean register(String username, String firstName, String lastName, String email, String password) throws DAOException, SQLException;

   boolean login(String username, String password) throws DAOException, SQLException;

   User getUserByUsername(String username) throws DAOException, SQLException;
}