package by.artem.payme.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnection {
   private final static String dbDriver = "com.mysql.jdbc.Driver";
   private final static String dbURL = "jdbc:mysql://localhost:3306/";
   private final static String dbName = "payments";
   private final static String dbParams = "?useTimezone=true&serverTimezone=UTC";
   private final static String dbUsername = "root";
   private final static String dbPassword = "root";

   private static Connection connection;

   private DbConnection() {
      initConnection();
   }

   public static void initConnection() {
      try {
         Class.forName(dbDriver);
         connection = DriverManager.getConnection(dbURL + dbName + dbParams, dbUsername, dbPassword);
      } catch (SQLException e) {
         throw new RuntimeException("Error connecting to db", e);
      } catch (ClassNotFoundException e) {
         throw new RuntimeException("Error finding db connection driver", e);
      }
   }

   public static Connection getInstance() {
      if (connection == null) {
         initConnection();
      }
      return connection;
   }
}
