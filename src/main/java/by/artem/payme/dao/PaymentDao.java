package by.artem.payme.dao;

import by.artem.payme.bean.Payment;
import by.artem.payme.dao.exception.DAOException;

import java.util.ArrayList;

public interface PaymentDao {
   ArrayList<Payment> getPaymentsByBankAccount(int accountNumber);

   Payment getPaymentById(int id) throws DAOException;

   boolean addDeposit(int accountNumber, int cardNumber, int amount) throws DAOException;
}
