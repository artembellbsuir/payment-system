package by.artem.payme.dao.impl;

import by.artem.payme.bean.Account;
import by.artem.payme.bean.Card;
import by.artem.payme.dao.CardDao;
import by.artem.payme.dao.DbConnection;
import by.artem.payme.dao.exception.DAOException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CardDaoImpl implements CardDao {
   private static final String tableName = "card";
   private Connection connection;

   public CardDaoImpl() {
      connection = DbConnection.getInstance();
   }

   @Override
   public boolean createCard(int customerId, int accountNumber, int number) throws DAOException {
      try {
         String sql = String.format("INSERT INTO %s (id, customerId, accountNumber, number) VALUES (NULL, ?, ?, ?);", tableName);

         PreparedStatement statement = connection.prepareStatement(sql);
         statement.setInt(1, customerId);
         statement.setInt(2, accountNumber);
         statement.setInt(3, number);

         boolean inserted = statement.executeUpdate() > 0;
         statement.close();

         return inserted;
      } catch (SQLException e) {
         throw new DAOException(e);
      }
   }


   @Override
   public ArrayList<Card> getBankAccountCards(int accountNumber) throws DAOException {
      try {
         String sql = String.format("SELECT * FROM %s WHERE accountNumber=?;", tableName);

         PreparedStatement statement = connection.prepareStatement(sql);
         statement.setInt(1, accountNumber);

         ResultSet set = statement.executeQuery();

         ArrayList<Card> cards = new ArrayList<>();
         Card card = null;
         if (set.next()) {
            card = new Card(
                    set.getInt("id"),
                    set.getInt("customerId"),
                    set.getInt("accountNumber"),
                    set.getInt("number")
            );
            cards.add(card);
         }

         return cards;
      } catch (SQLException e) {
         throw new DAOException(e);
      }
   }

   @Override
   public ArrayList<Card> getCardsByCustomerId(int customerId) throws DAOException {
      try {
         String sql = String.format("SELECT * FROM %s WHERE customerId=?;", tableName);

         PreparedStatement statement = connection.prepareStatement(sql);
         statement.setInt(1, customerId);

         ResultSet set = statement.executeQuery();

         ArrayList<Card> cards = new ArrayList<>();
         Card card = null;
         if (set.next()) {
            card = new Card(
                    set.getInt("id"),
                    set.getInt("customerId"),
                    set.getInt("accountNumber"),
                    set.getInt("number")
            );
            cards.add(card);
         }

         return cards;
      } catch (SQLException e) {
         throw new DAOException(e);
      }
   }

   @Override
   public Card getCardByCardNumber(int cardNumber) throws DAOException {
      try {
         String sql = String.format("SELECT * FROM %s WHERE number=?;", tableName);

         PreparedStatement statement = connection.prepareStatement(sql);
         statement.setInt(1, cardNumber);

         ResultSet set = statement.executeQuery();

         Card card = null;
         while (set.next()) {
            card = new Card(
                    set.getInt("id"),
                    set.getInt("customerId"),
                    set.getInt("accountNumber"),
                    set.getInt("number")
            );
         }

         return card;
      } catch (SQLException e) {
         throw new DAOException(e);
      }
   }

   @Override
   public boolean deleteCardById(int id) throws DAOException {
      try {
         String sql = String.format("DELETE FROM %s WHERE id=?;", tableName);

         PreparedStatement statement = connection.prepareStatement(sql);
         statement.setInt(1, id);

         boolean deleted = statement.executeUpdate() > 0;
         statement.close();

         return deleted;
      } catch (SQLException e) {
         throw new DAOException(e);
      }
   }
}
