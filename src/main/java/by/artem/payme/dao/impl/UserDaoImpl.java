package by.artem.payme.dao.impl;

import by.artem.payme.bean.User;
import by.artem.payme.dao.DbConnection;
import by.artem.payme.dao.UserDao;
import by.artem.payme.dao.exception.DAOException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDaoImpl implements UserDao {

   private static final String tableName = "user";
   private Connection connection;

   public UserDaoImpl() {
      connection = DbConnection.getInstance();
   }

   @Override
   public boolean register(String username, String firstName, String lastName, String email, String password) throws DAOException, SQLException {
      try {
         String sql = String.format("INSERT INTO %s (id, username, firstName, lastName, email, password) " +
                 "VALUES (NULL, ?, ?, ?, ?, ?);", tableName);

         PreparedStatement statement = connection.prepareStatement(sql);
         statement.setString(1, username);
         statement.setString(2, firstName);
         statement.setString(3, lastName);
         statement.setString(4, email);
         statement.setString(5, password);

         boolean inserted = statement.executeUpdate() > 0;
         statement.close();

         return inserted;
      } catch (SQLException e) {
         throw new DAOException("Error while registering a new user", e);
      }
   }

   @Override
   public boolean login(String login, String password) throws DAOException, SQLException {
      String sql = String.format("SELECT * FROM %s WHERE username=? AND password=?;", tableName);

      PreparedStatement statement = connection.prepareStatement(sql);
      statement.setString(1, login);
      statement.setString(2, password);

      ResultSet set = statement.executeQuery();

      return set.next();
   }

   public User getUserByUsername(String username) throws SQLException {
      String sql = String.format("SELECT * FROM %s WHERE username=?;", tableName);

      PreparedStatement statement = connection.prepareStatement(sql);
      statement.setString(1, username);

      ResultSet set = statement.executeQuery();

      User user = null;
      if (set.next()) {
         user = new User(
                 set.getInt("id"),
                 set.getString("username"),
                 set.getString("firstName"),
                 set.getString("lastName"),
                 set.getString("email"),
                 set.getString("role")
         );
      }

      return user;
   }


}