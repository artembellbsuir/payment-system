package by.artem.payme.dao.impl;

import by.artem.payme.bean.Account;
import by.artem.payme.bean.Card;
import by.artem.payme.bean.User;
import by.artem.payme.dao.AccountDao;
import by.artem.payme.dao.DbConnection;
import by.artem.payme.dao.exception.DAOException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AccountDaoImpl implements AccountDao {
   private static final String tableName = "account";
   private final Connection connection;

   public AccountDaoImpl() {
      connection = DbConnection.getInstance();
   }

   @Override
   public boolean createAccount(int customerId, int accountNumber, int balance, boolean isBlocked) throws SQLException, DAOException {
      try {
         String sql = String.format("INSERT INTO %s (id, customerId, accountNumber, balance, isBlocked) VALUES (NULL, ?, ?, ?, ?);", tableName);

         PreparedStatement statement = connection.prepareStatement(sql);
         statement.setInt(1, customerId);
         statement.setInt(2, accountNumber);
         statement.setInt(3, balance);
         statement.setBoolean(4, isBlocked);

         boolean inserted = statement.executeUpdate() > 0;
         statement.close();

         return inserted;
      } catch (SQLException e) {
         throw new DAOException(e);
      }
   }

   @Override
   public ArrayList<Account> getAccountsByCustomerId(int customerId) throws DAOException {
      try {
         String sql = String.format("SELECT * FROM %s WHERE customerId=?;", tableName);

         PreparedStatement statement = connection.prepareStatement(sql);
         statement.setInt(1, customerId);

         ResultSet set = statement.executeQuery();

         ArrayList<Account> accounts = new ArrayList<>();
         Account account = null;
         while (set.next()) {
            account = new Account(
                    set.getInt("id"),
                    set.getInt("customerId"),
                    set.getInt("accountNumber"),
                    set.getInt("balance"),
                    set.getBoolean("isBlocked")
            );
            accounts.add(account);
         }

         return accounts;
      } catch (SQLException e) {
         throw new DAOException(e);
      }
   }


   @Override
   public Account getBankAccountByNumber(int accountNumber) throws DAOException {
      try {
         String sql = String.format("SELECT * FROM %s WHERE accountNumber=?;", tableName);

         PreparedStatement statement = connection.prepareStatement(sql);
         statement.setInt(1, accountNumber);

         ResultSet set = statement.executeQuery();

         Account account = null;
         if (set.next()) {
            account = new Account(
                    set.getInt("id"),
                    set.getInt("customerId"),
                    set.getInt("accountNumber"),
                    set.getInt("balance"),
                    set.getBoolean("isBlocked")
            );
         }

         return account;
      } catch ( SQLException e) {
         throw new DAOException(e);
      }
   }

   @Override
   public boolean changBlockedAccountByNumber(int accountNumber, boolean isBlocked) throws DAOException {
      try {
         String sql = String.format("UPDATE %s SET is_blocked=? WHERE account_number=?;", tableName);

         PreparedStatement statement = connection.prepareStatement(sql);
         statement.setBoolean(1, isBlocked);
         statement.setInt(2, accountNumber);

         ResultSet set = statement.executeQuery();

         boolean updated = statement.executeUpdate() > 0;
         statement.close();

         return updated;
      } catch (SQLException e) {
         throw new DAOException(e);
      }
   }

   @Override
   public boolean deleteBankAccountById(int id) throws DAOException {
      try {
         String sql = String.format("DELETE FROM %s WHERE id=?;", tableName);

         PreparedStatement statement = connection.prepareStatement(sql);
         statement.setInt(1, id);

         boolean deleted = statement.executeUpdate() > 0;
         statement.close();

         return deleted;
      } catch (SQLException e) {
         throw new DAOException(e);
      }
   }
}
