package by.artem.payme.dao.impl;

import by.artem.payme.bean.Card;
import by.artem.payme.bean.Payment;
import by.artem.payme.dao.DbConnection;
import by.artem.payme.dao.PaymentDao;
import by.artem.payme.dao.exception.DAOException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class PaymentDaoImpl implements PaymentDao {
   private static final String tableName = "payment";
   private Connection connection;

   public PaymentDaoImpl() {
      connection = DbConnection.getInstance();
   }

   @Override
   public ArrayList<Payment> getPaymentsByBankAccount(int accountNumber) {
      return null;
   }

   @Override
   public Payment getPaymentById(int id) throws DAOException {
      try {
         String sql = String.format("SELECT * FROM %s WHERE id=?;", tableName);

         PreparedStatement statement = connection.prepareStatement(sql);
         statement.setInt(1, id);

         ResultSet set = statement.executeQuery();

         Payment payment = null;
         if (set.next()) {
            payment = new Payment(
                    set.getInt("id"),
                    set.getInt("destAccountNumber"),
                    set.getInt("cardNumber"),
                    set.getInt("amount")
            );
         }

         return payment;
      } catch (SQLException e) {
         throw new DAOException(e);
      }
   }

   @Override
   public boolean addDeposit(int accountNumber, int cardNumber, int amount) throws DAOException {
      try {
         String sql = String.format("INSERT INTO %s VALUES (NULL, ?, ?, ?);", tableName);

         PreparedStatement statement = connection.prepareStatement(sql);
         statement.setInt(1, accountNumber);
         statement.setInt(2, cardNumber);
         statement.setInt(3, amount);

         ResultSet set = statement.executeQuery();

         boolean inserted = statement.executeUpdate() > 0;
         statement.close();

         return inserted;
      } catch (SQLException e) {
         throw new DAOException(e);
      }
   }
}
