package by.artem.payme.dao;

import by.artem.payme.bean.Account;
import by.artem.payme.bean.Card;
import by.artem.payme.dao.exception.DAOException;

import java.sql.SQLException;
import java.util.ArrayList;

public interface AccountDao {
   public boolean createAccount(int customerId, int accountNumber, int balance, boolean isBlocked) throws SQLException, DAOException;

   ArrayList<Account> getAccountsByCustomerId(int customerId) throws DAOException;

   Account getBankAccountByNumber(int accountNumber) throws DAOException;

   boolean changBlockedAccountByNumber(int accountNumber, boolean isBlocked) throws DAOException;

   boolean deleteBankAccountById(int id) throws DAOException;

}
