package by.artem.payme.dao;

import by.artem.payme.dao.impl.AccountDaoImpl;
import by.artem.payme.dao.impl.CardDaoImpl;
import by.artem.payme.dao.impl.PaymentDaoImpl;
import by.artem.payme.dao.impl.UserDaoImpl;

public class DAOFactory {
   private static final DAOFactory instance = new DAOFactory();

   private final UserDao userDAO = new UserDaoImpl();
   private final AccountDao accountDao = new AccountDaoImpl();
   private final CardDao cardDao = new CardDaoImpl();
   private final PaymentDao paymentDao = new PaymentDaoImpl();

   private DAOFactory() {
   }

   public static DAOFactory getInstance() {
      return instance;
   }

   public UserDao getUserDAO() {
      return userDAO;
   }

   public AccountDao getAccountDao() {
      return accountDao;
   }

   public CardDao getCardDao() {
      return cardDao;
   }

   public PaymentDao getPaymentDao() {
      return paymentDao;
   }

}