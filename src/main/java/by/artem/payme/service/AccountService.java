package by.artem.payme.service;

import by.artem.payme.bean.Account;
import by.artem.payme.bean.Card;
import by.artem.payme.dao.exception.DAOException;
import by.artem.payme.service.exception.ServiceException;

import java.util.ArrayList;

public interface AccountService {
   public boolean createAccount(int customerId, int accountNumber, int balance, boolean isBlocked) throws ServiceException;

   boolean addDeposit(int accountNumber, int amount);

   ArrayList<Account> getAccountsByCustomerId(int customerId) throws ServiceException;

   Account getBankAccountByNumber(int accountNumber);

   ArrayList<Card> getBankAccountCards(int accountNumber);

   boolean blockBankAccountByNumber(int accountNumber);
}
