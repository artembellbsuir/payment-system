package by.artem.payme.service;

import by.artem.payme.bean.Account;
import by.artem.payme.service.impl.AccountServiceImpl;
import by.artem.payme.service.impl.CardServiceImpl;
import by.artem.payme.service.impl.PaymentServiceImpl;
import by.artem.payme.service.impl.UserServiceImpl;

public class ServiceFactory {
   private static final ServiceFactory instance = new ServiceFactory();

   private final UserService userService = new UserServiceImpl();
   private final AccountService accountService = new AccountServiceImpl();
   private final CardService cardService = new CardServiceImpl();
   private final PaymentService paymentService = new PaymentServiceImpl();

   public static ServiceFactory getInstance() {
      return instance;
   }

   public UserService getUserService() {
      return userService;
   }

   public AccountService getAccountService() {
      return accountService;
   }

   public CardService getCardService() {
      return cardService;
   }

   public PaymentService getPaymentService() {
      return paymentService;
   }
}
