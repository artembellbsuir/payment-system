package by.artem.payme.service.impl;

import by.artem.payme.service.FieldValidatorService;

public class FieldValidatorServiceImpl {
   public static boolean validateString(String value) {
      return value != null && !value.trim().equals("");
   }

   public static boolean validateNumber(String value) {
      return isNumeric(value) && Integer.parseInt(value) > 0;
   }

   public static boolean validatePassword(String value) {
      return validateString(value) && value.length() >= 3;
   }

   private static boolean isNumeric(String value) {
      if (value == null) {
         return false;
      }
      try {
         double d = Double.parseDouble(value);
      } catch (NumberFormatException nfe) {
         return false;
      }
      return true;
   }
}
