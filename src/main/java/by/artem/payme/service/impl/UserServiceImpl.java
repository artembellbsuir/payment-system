package by.artem.payme.service.impl;

import by.artem.payme.bean.User;
import by.artem.payme.dao.DAOFactory;
import by.artem.payme.dao.UserDao;
import by.artem.payme.dao.exception.DAOException;
import by.artem.payme.service.UserService;
import by.artem.payme.service.exception.ServiceException;

import java.sql.SQLException;

public class UserServiceImpl implements UserService {

   @Override
   public boolean login(String username, String password) throws ServiceException {
      DAOFactory daoFactory = DAOFactory.getInstance();
      UserDao userDAO = daoFactory.getUserDAO();

      try {
         return userDAO.login(username, password);
      } catch (DAOException e) {
         throw new ServiceException("Error while login", e);
      } catch (SQLException e) {
         throw new ServiceException("Error connecting to db", e);
      }
   }

   @Override
   public boolean register(String login, String firstName, String lastName, String email, String password) throws ServiceException {
      DAOFactory daoFactory = DAOFactory.getInstance();
      UserDao userDAO = daoFactory.getUserDAO();

      try {
         return userDAO.register(login, firstName, lastName, email, password);
      } catch (DAOException e) {
         throw new ServiceException("Error while registering", e);
      } catch (SQLException e) {
         throw new ServiceException("Error connecting to db", e);
      }
   }

   public User getUserByUsername(String username) throws ServiceException {
      DAOFactory daoFactory = DAOFactory.getInstance();
      UserDao userDAO = daoFactory.getUserDAO();

      try {
         return userDAO.getUserByUsername(username);
      } catch (DAOException e) {
         throw new ServiceException("Error getting user by username", e);
      } catch (SQLException e) {
         throw new ServiceException("Error connecting to db", e);
      }
   }
}
