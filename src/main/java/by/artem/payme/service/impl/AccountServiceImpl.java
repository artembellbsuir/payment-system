package by.artem.payme.service.impl;

import by.artem.payme.bean.Account;
import by.artem.payme.bean.Card;
import by.artem.payme.dao.AccountDao;
import by.artem.payme.dao.DAOFactory;
import by.artem.payme.dao.UserDao;
import by.artem.payme.dao.exception.DAOException;
import by.artem.payme.service.AccountService;
import by.artem.payme.service.exception.ServiceException;

import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.ArrayList;

public class AccountServiceImpl implements AccountService {
   @Override
   public boolean createAccount(int customerId, int accountNumber, int balance, boolean isBlocked) throws ServiceException {
      DAOFactory daoFactory = DAOFactory.getInstance();
      AccountDao accountDao = daoFactory.getAccountDao();


      try {
         return accountDao.createAccount(customerId, accountNumber, balance, isBlocked);
      } catch (DAOException | SQLException e) {
         throw new ServiceException("Error while logination", e);
      }
   }

   @Override
   public boolean addDeposit(int accountNumber, int amount) {
      return false;
   }

   @Override
   public ArrayList<Account> getAccountsByCustomerId(int customerId) throws ServiceException {
      DAOFactory daoFactory = DAOFactory.getInstance();
      AccountDao accountDao = daoFactory.getAccountDao();


      try {
         return accountDao.getAccountsByCustomerId(customerId);
      } catch (DAOException e) {
         throw new ServiceException("Error while logination", e);
      }
   }

   @Override
   public Account getBankAccountByNumber(int accountNumber) {
      return null;
   }

   @Override
   public ArrayList<Card> getBankAccountCards(int accountNumber) {
      return null;
   }

   @Override
   public boolean blockBankAccountByNumber(int accountNumber) {
      return false;
   }
}
