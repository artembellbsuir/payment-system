package by.artem.payme.service.impl;

import by.artem.payme.bean.Account;
import by.artem.payme.bean.Card;
import by.artem.payme.dao.AccountDao;
import by.artem.payme.dao.CardDao;
import by.artem.payme.dao.DAOFactory;
import by.artem.payme.dao.exception.DAOException;
import by.artem.payme.service.CardService;
import by.artem.payme.service.exception.ServiceException;

import java.util.ArrayList;

public class CardServiceImpl implements CardService {
   @Override
   public boolean createCard(int customerId, int accountNumber, int number) throws ServiceException {

      DAOFactory daoFactory = DAOFactory.getInstance();
      CardDao cardDao = daoFactory.getCardDao();


      try {
         return cardDao.createCard(customerId, accountNumber, number);
      } catch (DAOException e) {
         throw new ServiceException("Error while logination", e);
      }
   }

   @Override
   public ArrayList<Card> getCardsByCustomerId(int customerId) throws ServiceException {
      DAOFactory daoFactory = DAOFactory.getInstance();
      CardDao cardDao = daoFactory.getCardDao();


      try {
         return cardDao.getCardsByCustomerId(customerId);
      } catch (DAOException e) {
         throw new ServiceException("Error while logination", e);
      }
   }

   @Override
   public boolean getCardByCardNumber(int cardNumber) {
      return false;
   }

   @Override
   public boolean deleteCardById(int id) {
      return false;
   }
}
