package by.artem.payme.service;

import by.artem.payme.bean.User;
import by.artem.payme.service.exception.ServiceException;

public interface UserService {
   boolean login(String username, String password) throws ServiceException;

   boolean register(String username, String firstName, String lastName, String email, String password) throws ServiceException;

   User getUserByUsername(String username) throws ServiceException;
}