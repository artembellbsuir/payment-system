package by.artem.payme.service;

import by.artem.payme.bean.Payment;

import java.util.ArrayList;

public interface PaymentService {
   ArrayList<Payment> getPaymentsByBankAccount(int accountNumber);

   Payment getPaymentById(int id);
}
