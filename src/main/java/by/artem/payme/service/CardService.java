package by.artem.payme.service;

import by.artem.payme.bean.Account;
import by.artem.payme.bean.Card;
import by.artem.payme.service.exception.ServiceException;

import java.util.ArrayList;

public interface CardService {
   boolean createCard(int customerId, int accountNumber, int number) throws ServiceException;

   ArrayList<Card> getCardsByCustomerId(int customerId) throws ServiceException;

   boolean getCardByCardNumber(int cardNumber);


   boolean deleteCardById(int id);
}
