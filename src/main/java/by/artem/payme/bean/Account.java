package by.artem.payme.bean;

public class Account {
   private int id;

   private int customerId;
   private int accountNumber;
   private int balance;
   private boolean isBlocked;

   public Account() {
   }

   public Account(int id, int customerId, int accountNumber, int balance, boolean isBlocked) {
      this.id = id;
      this.customerId = customerId;
      this.accountNumber = accountNumber;
      this.balance = balance;
      this.isBlocked = isBlocked;
   }

   public int getCustomerId() {
      return this.customerId;
   }

   public int getAccountNumber() {
      return this.accountNumber;
   }

   public int getBalance() {
      return this.balance;
   }

   public boolean getIsBlocked() {
      return this.isBlocked;
   }
}
