package by.artem.payme.bean;

public class User {

   private int id;

   private String username;
   private String firstName;
   private String lastName;
   private String email;
   private String role;

   public User() {
   }

   public User(int id, String username, String firstName, String lastName, String email, String role) {
      this.id = id;
      this.username = username;
      this.firstName = firstName;
      this.lastName = lastName;
      this.email = email;
      this.role = role;
   }

   public int getId() {
      return this.id;
   }

   public String getPassword() {
      return this.email;
   }

   public String getUsername() {
      return this.username;
   }
}
