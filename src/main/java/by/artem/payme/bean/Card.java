package by.artem.payme.bean;

public class Card {
   private int id;

   private final int customerId;
   private final int accountNumber;
   private final int number;

   public Card(int id, int customerId, int accountNumber, int number) {
      this.id = id;
      this.customerId = customerId;
      this.accountNumber = accountNumber;
      this.number = number;
   }

   public int getCustomerId() {
      return this.customerId;
   }

   public int getAccountNumber() {
      return this.accountNumber;
   }

   public int getNumber() {
      return this.number;
   }
}
