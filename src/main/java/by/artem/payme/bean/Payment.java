package by.artem.payme.bean;

public class Payment {
   private int id;

   private int destAccountNumber;
   private int cardNumber;
   private int amount;

   public Payment(int id, int destAccountNumber, int cardNumber, int amount) {
      this.id = id;
      this.destAccountNumber = destAccountNumber;
      this.cardNumber = cardNumber;
      this.amount = amount;
   }

   public int getDestAccountNumber() {
      return this.destAccountNumber;
   }

   public int getCardNumber() {
      return this.cardNumber;
   }

   public int getAmount() {
      return this.amount;
   }
}
