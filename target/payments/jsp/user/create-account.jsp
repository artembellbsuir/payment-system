<%--
  Created by IntelliJ IDEA.
  User: artembell
  Date: 13.12.2020
  Time: 10:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create account</title>
</head>
<body>
<h1>Create account</h1>
<p style = "color:red;"><%=request.getAttribute("error-text")%></p>
<form action="controller?command=create-account" method="post">
    Please enter account details:<br>
    Initial balance: <input type="number" name="balance"><br>
    <input type="submit">
</form>
</body>
</html>
