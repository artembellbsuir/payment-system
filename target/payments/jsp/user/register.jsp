<%--
  Created by IntelliJ IDEA.
  User: artembell
  Date: 11.12.2020
  Time: 13:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="by.artem.payme.command.CommandType" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration</title>
</head>
<body>
<h1>Registration</h1>
<form action="controller?command=register" method="post">
    Please enter username and password:<br>
    Username: <input type="text" name="username"><br>
    First Name <input type="text" name="firstName"><br>
    Last Name <input type="text" name="lastName"><br>
    Email: <input type="email" name="email"><br>
    Password <input type="password" name="password"><br>
    <input type="submit">

    or <a href="controller?command=login">login</a>
</form>
</body>
</html>
