<%--
  Created by IntelliJ IDEA.
  User: artembell
  Date: 11.12.2020
  Time: 13:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="by.artem.payme.command.CommandType" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
<h1>Login</h1>
<p style = "color:red;"><%=request.getAttribute("error-text")%></p>
<form action="controller?command=login" method="post">
    Please enter username and password:<br>
    Username: <input type="text" name="username"><br>
    Password <input type="text" name="password"><br>
    <input type="submit">
    or <a href="controller?command=register">register</a>
</form>
</body>
</html>