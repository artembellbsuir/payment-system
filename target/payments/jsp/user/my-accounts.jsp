<%@ page import="by.artem.payme.bean.Account" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: artembell
  Date: 12.12.2020
  Time: 19:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>My accounts</title>
</head>
<body>
    <h1>My accounts</h1>

    <a href="controller?command=create-account">create account</a>

    <p style = "color:red;"><%= request.getAttribute("error-text")%></p>

    <%ArrayList<Account> accounts = (ArrayList<Account>)request.getAttribute("accounts");
        for(Account acc : accounts) {%>

        <p>
            <h4>Account number: <%=acc.getAccountNumber()%></h4>
            <h4>Balance: <%=acc.getBalance()%></h4>
            <h4>Status: <%=acc.getIsBlocked() ? "Blocked" : "Not blocked"%></h4>
        </p>
        <hr>
    <%}%>
</body>
</html>
